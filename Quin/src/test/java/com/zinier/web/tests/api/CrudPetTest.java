/**
 * 
 */
package com.zinier.web.tests.api;

import org.testng.annotations.Test;

import com.zinier.api.data.service.CreatePetService;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;


/**
 * @author Durgapraveen Tiyyagura
 *
 */
public class CrudPetTest extends CreatePetService {
	int id;
	String mobileTaskid;
	String resumeKey;

	@Test(priority = 0)
	public void createPetTest() {
		Response response = createPet();
		ResponseBody resp2 = response.getBody();
		id = resp2.jsonPath().get("id");
		softAssert.assertEquals(response.getStatusCode(),200);
		softAssert.assertAll();
	}

	@Test(priority = 1)
	public void fetchPetTest() {
		Response response = getPet(id);
		ResponseBody<?> body = response.getBody();
		softAssert.assertEquals(response.getStatusCode(),200);
		softAssert.assertAll();
	}

	@Test(priority = 2)
	public void updatePetTest() {
		Response response = updatePet(id);
		ResponseBody<?> body = response.getBody();
		softAssert.assertEquals(response.getStatusCode(),200);
		softAssert.assertAll();
	}

	@Test(priority = 3)
	public void DeletePetTest() {
		Response response = deletePet(id);
		ResponseBody<?> body = response.getBody();
		softAssert.assertEquals(response.getStatusCode(),200);
		softAssert.assertAll();
	}

}
