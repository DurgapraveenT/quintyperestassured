/**
 * 
 */
package com.zinier.web.tests.api;

import org.testng.annotations.Test;

import com.zinier.api.data.service.CreateUserService;

import io.restassured.response.Response;
import io.restassured.response.ResponseBody;


/**
 * @author Durgapraveen Tiyyagura
 *
 */
public class CrudUsersTest extends CreateUserService {
	int id;
	String mobileTaskid;
	String resumeKey;
	String uName;

	@Test(priority = 0)
	public void createUserTest() {
		Response response = createUser();
		ResponseBody resp2 = response.getBody();
		softAssert.assertEquals(response.getStatusCode(),200);
		softAssert.assertAll();
	}

	@Test(priority = 1)
	public void getUserTest() {
		Response response = getUser();
		ResponseBody<?> body = response.getBody();
		softAssert.assertEquals(response.getStatusCode(),200);
		softAssert.assertAll();
	}

	@Test(priority = 2)
	public void loginUserTest() {
		Response response = loginUser();
		ResponseBody<?> body = response.getBody();
		softAssert.assertEquals(response.getStatusCode(),200);
		softAssert.assertAll();
	}

	@Test(priority = 3)
	public void logoutUserTest() {
		Response response = logoutUser();
		ResponseBody<?> body = response.getBody();
		softAssert.assertEquals(response.getStatusCode(),200);
		softAssert.assertAll();
	}

}
