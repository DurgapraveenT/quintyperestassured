package com.zinier.api.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.commons.configuration.CombinedConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.tree.OverrideCombiner;
import org.slf4j.LoggerFactory;

public class PropertyUtil {

	public static Properties props;
	public static final String runtimeProperty = "resources/properties/runtime.properties";
	public static final String endpointProperties = "resources/properties/endPoints.properties";

	private static CombinedConfiguration combinedConfig = new CombinedConfiguration(new OverrideCombiner());

	public static void appendProperties(String propertiesFile) {
		PropertiesConfiguration properties = null;

		try {
			LoggerFactory.getLogger(PropertyUtil.class).info("Loading property file:" + propertiesFile);
			properties = new PropertiesConfiguration(propertiesFile);
			properties.setDelimiterParsingDisabled(true);
			LoggerFactory.getLogger(PropertyUtil.class).info("Properties file loaded:" + propertiesFile);

		} catch (ConfigurationException ex) {
			System.out.println(ex);
		}
		if (properties != null) {
			combinedConfig.addConfiguration(properties);
		}

	}

	public static int getInt(String key) {
		return combinedConfig.getInt(key);
	}

	public static List<Object> getListOfObjects(String key) {
		return combinedConfig.getList(key);
	}

	public static Long getLong(String key) {
		return combinedConfig.getLong(key);
	}

	public static String getString(String key) {

		return combinedConfig.getString(key);
	}

	public static String[] getStringArray(String key) {
		return combinedConfig.getStringArray(key);
	}

	/**
	 * Singleton instance
	 * 
	 * @throws IOException
	 */

	/*
	 * Delete property Value
	 * 
	 */
	public void deleteProperties(String propertyFilePath, String Key) {
		try {
			FileInputStream fin = new FileInputStream("foldername.properties");
			FileOutputStream fout = new FileOutputStream("foldername.properties");
			Properties props = new Properties();
			props.load(fin);
			props.remove(Key);
			props.store(fout, "updated");
		} catch (Exception e) {
			System.out.println("Exception occured " + e);
		}

	}

	// Update property file based on the input key

	public static void updatePropertyFile(String propertyFilePath, String Key, String value) {
		try {
			FileInputStream in = new FileInputStream(propertyFilePath);
			Properties props = new Properties();
			props.load(in);
			in.close();

			FileOutputStream out = new FileOutputStream(propertyFilePath);
			props.setProperty(Key, value);
			props.store(out, null);
			out.close();
		} catch (Exception e) {
			System.out.println("Exception occured " + e);
		}

	}

	/**
	 * Reads a properties (value) file into a java.util.Properties object.
	 * 
	 * @param filename
	 * 
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static Properties readPropertyFile(String propertyFilePath) {
		Properties p = null;
		FileReader fr = null;

		try {
			p = new Properties();
			fr = new FileReader(propertyFilePath);
			p.load(fr);
//			p.getProperty(Key);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fr.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return p;
	}

	public static String getPropertyKey(String propertyFilePath, String key) {
		Properties p = null;
		FileReader fr = null;
		String keyValue = "";
		try {
			p = new Properties();
			fr = new FileReader(propertyFilePath);
			p.load(fr);
			keyValue = p.getProperty(key);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				fr.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return keyValue;
	}

	/**
	 * Creates a properties file (key=value), filename, given a java.util.Properties
	 * object
	 * 
	 * @param filename
	 * @param properties
	 * @throws IOException
	 */

	public void createPropertyFile(String filename, Properties properties) {
		FileWriter fw = null;

		try {
			fw = new FileWriter(filename);
			properties.store(fw, "Properties file created on " + new java.util.Date().toString());
		} catch (Exception e) {
			System.out.println("Exception occured " + e);
		} finally {
			try {
				fw.close();
			} catch (IOException e) {
				System.out.println("Exception occured " + e);
			}
		}

	}

	public String getFilePath(String key) {
		return PropertyUtil.readPropertyFile("resources/properties/csvPath.properties").getProperty(key);
	}

	public String getPropertyValue(String filePath, String key) {
		return PropertyUtil.readPropertyFile(filePath).getProperty(key);
	}

}
