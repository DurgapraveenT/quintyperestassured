package com.zinier.api.data.modals;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.testng.annotations.BeforeClass;

import org.testng.asserts.SoftAssert;

import com.zinier.api.utils.PropertyUtil;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static io.restassured.RestAssured.*;

public class APIBaseTest {

	public ResponseSpecification responseSpecification;
	public RequestSpecification requestSpecification;
	public static final String endPointProperties = "resources/properties/endPoints.properties";
	public static Properties props = PropertyUtil.readPropertyFile(endPointProperties);
	public SoftAssert softAssert = new SoftAssert();


	@BeforeClass(alwaysRun=true)
	public void APIBase() {

		globalRequestSpecificationBuilder(baseURI);
		globalResponseSpecificationBuilder();	
	}

	private void globalResponseSpecificationBuilder() {
		
		HashMap<String, Object> reponseHeaders = new HashMap<String, Object>();
		ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder();
		responseSpecBuilder.expectHeaders(reponseHeaders);
		responseSpecification = responseSpecBuilder.build();
	}

	private void globalRequestSpecificationBuilder(String baseURI) {
		Map<String, String> headersMap = new HashMap<String, String>();
		RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
		headersMap.put("loginid", props.getProperty("loginid"));
		headersMap.put("loglevel", props.getProperty("loglevel"));
		
		headersMap.put("Content-Type", "application/json");
		requestSpecBuilder.addHeaders(headersMap);
		requestSpecBuilder.setBaseUri(props.getProperty("baseURI"));
		requestSpecBuilder.setRelaxedHTTPSValidation();
		requestSpecification = requestSpecBuilder.build();
		System.out.println("Thread: " + Thread.currentThread().getId());
	}
}