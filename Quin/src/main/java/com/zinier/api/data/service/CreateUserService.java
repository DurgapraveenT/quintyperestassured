/**
 * 
 */
package com.zinier.api.data.service;

import static io.restassured.RestAssured.given;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.apache.hc.core5.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;
import com.zinier.api.data.modals.APIBaseTest;

import io.restassured.response.Response;

/**
 * @author Durgapraveen Tiyyagura
 *
 */
public class CreateUserService extends APIBaseTest{

	String basePath;
	Object obj = "";
	String userName;
	String password;

	public Response createUser() {
		basePath = props.getProperty("createUser");
		JSONParser parser = new JSONParser();
		try {
			String usersFile = this.getClass().getClassLoader()
					.getResource("com.zinier.web.tests.api/userInputs.json").getPath();
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(usersFile), "UTF8"));
			obj = parser.parse(reader);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		String createUser = new Gson().toJson(obj);
		JSONObject jsonObject = new JSONObject(createUser);
		JSONArray createUserjson =jsonObject.getJSONArray("CreateUser");
		JSONObject userjsonObj = createUserjson.getJSONObject(0);
		
		System.out.println("uName1" +userjsonObj.toString());
		userName =userjsonObj.getString("username");
		password =userjsonObj.getString("password");
		String createUserJsonString = createUserjson.toString();
		System.out.println("CreateUser request payload is" + createUserJsonString);
		Response response = given().spec(requestSpecification).basePath(basePath).log().everything().body(createUserJsonString)
				.when().post("").then().assertThat().statusCode(HttpStatus.SC_OK).spec(responseSpecification).extract().response();
		System.out.println("CreateUser response --> "+response.asString());
		return response;
	}

	public Response getUser() {
		basePath = props.getProperty("getUser");
		Response response = given().spec(requestSpecification).basePath(basePath + "/"+userName).log().everything().body("")
				.when().get().then().assertThat().statusCode(HttpStatus.SC_OK).spec(responseSpecification).extract().response();
		System.out.println("getPet response--> "+response.asString());
		return response;
	}

	public Response loginUser() {
		basePath = props.getProperty("loginUser");
		
		Response response = given().spec(requestSpecification).basePath(basePath + "?userName="+userName+ "&password="+password).log().everything().body("")
				.when().get("").then().assertThat().statusCode(HttpStatus.SC_OK).spec(responseSpecification).extract().response();
		System.out.println("loginUser response     "  +response.asString());
		return response;
	}
	public Response logoutUser() {
		basePath = props.getProperty("logoutUser");
		Response response = given().spec(requestSpecification).basePath(basePath).log().everything().body("")
				.when().get().then().assertThat().statusCode(HttpStatus.SC_OK).spec(responseSpecification).extract().response();
		System.out.println("deletePet response--> "+response.asString());
		return response;
	}
}