/**
 * 
 */
package com.zinier.api.data.service;

import static io.restassured.RestAssured.given;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.apache.hc.core5.http.HttpStatus;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.Gson;
import com.zinier.api.data.modals.APIBaseTest;

import io.restassured.response.Response;

/**
 * @author Durgapraveen Tiyyagura
 *
 */
public class CreatePetService extends APIBaseTest{

	String basePath;
	Object obj = "";

	public Response createPet() {
		basePath = props.getProperty("createPet");
		JSONParser parser = new JSONParser();
		try {
			String petsFile = this.getClass().getClassLoader()
					.getResource("com.zinier.web.tests.api/petInputs.json").getPath();
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(petsFile), "UTF8"));
			obj = parser.parse(reader);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		String createPet = new Gson().toJson(obj);
		JSONObject jsonObject = new JSONObject(createPet);
		JSONObject createPetjson =jsonObject.getJSONObject("CreatePet");
		String createPetJsonString = createPetjson.toString();
		System.out.println("createPet request payload is" + createPetJsonString);
		Response response = given().spec(requestSpecification).basePath(basePath).log().everything().body(createPetJsonString)
				.when().post("").then().assertThat().statusCode(HttpStatus.SC_OK).spec(responseSpecification).extract().response();
		System.out.println("createPet response --> "+response.asString());
		return response;
	}

	public Response getPet(int id) {
		basePath = props.getProperty("getPet");
		Response response = given().spec(requestSpecification).basePath(basePath + "/"+id).log().everything().body("")
				.when().get().then().assertThat().statusCode(HttpStatus.SC_OK).spec(responseSpecification).extract().response();
		System.out.println("getPet response--> "+response.asString());
		return response;
	}

	public Response updatePet(int id) {
		basePath = props.getProperty("updatePet");
		String updatePet = new Gson().toJson(obj);
		JSONObject jsonObject = new JSONObject(updatePet);
		JSONObject updatePetjson =jsonObject.getJSONObject("updatePet");
		updatePetjson.put("id", id);
		String updatePetjsonToString = updatePetjson.toString();

		System.out.println("updatePetjsonString     "  +updatePetjsonToString);
		System.out.println("updatePetjsonString payload is" + updatePetjsonToString);
		Response response = given().spec(requestSpecification).basePath(basePath).log().everything().body(updatePetjsonToString)
				.when().put("").then().assertThat().statusCode(HttpStatus.SC_OK).spec(responseSpecification).extract().response();
		System.out.println("updatePet response     "  +response.asString());
		return response;
	}
	public Response deletePet(int id) {
		basePath = props.getProperty("deletePet");
		Response response = given().spec(requestSpecification).basePath(basePath + "/"+id).log().everything().body("")
				.when().delete().then().assertThat().statusCode(HttpStatus.SC_OK).spec(responseSpecification).extract().response();
		System.out.println("deletePet response--> "+response.asString());
		return response;
	}
}